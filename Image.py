#! /usr/bin/env python3
# -*- coding:utf-8 -*-

# Author by Alex
import numpy as np
from typing import Callable, Self
import matplotlib.pyplot as plt
from utils.imath import decoordinate, getLocation, flip, calcDistanceMap


class Image(object):
    def __init__(self, array=np.array(0), border='mirror'):
        self.border = border
        self.array = array
        self.height, self.width = array.shape
        self.sampler = [i for i in range(self.width * self.height)]  # a random index list of the array
        self.counter = 0
        np.random.shuffle(self.sampler)
        self.distanceMap = None
        self.gradientMap = None

    def getSample(self):  # traverse all pixels in random order.

        if self.sampler[-1] == -1:
            self.rebuildSampler()

        if not self.sampler:
            return None, None

        for i, e in enumerate(self.sampler):
            if e == -1: continue  # skip used pixels
            x, y = decoordinate(self.width, e)
            if self.getItem(x, y) > 0:  # expected 1, try avoiding hard code for binary image.
                self.sampler[i] = -1  # mark sampled pixels
                return x, y

        return None, None

    def rebuildSampler(self):
        self.sampler.clear()
        for i in range(self.height):
            for j in range(self.width):
                if self.array[i][j] > 0:
                    self.sampler.append(i * self.width + j)
        np.random.shuffle(self.sampler)

    def getItem(self, x, y) -> int:
        return self._getItem(x, y, self.border)

    def _getItem(self, x, y, typ, c=None):

        a, b = getLocation((x, y), self.array.shape, typ)

        if a is None:  # for const typ
            return c

        return self.array[a][b]

    def setItem(self, x, y, value):

        if x < 0 or x >= self.height:
            return
        if y < 0 or y >= self.width:
            return

        self.array[x][y] = value

    def applyPublicFunction(self, func: Callable[[int, int, Self], int]):
        res = np.zeros((self.height, self.width), dtype=int)
        for i in range(self.height):
            for j in range(self.width):
                res[i][j] = func(i, j, self)
        return Image(res)

    def applyPrivateFunction(self, func: Callable[[int, int], float]):
        res = np.zeros((self.height, self.width), dtype=float)
        for i in range(self.height):
            for j in range(self.width):
                res[i][j] = func(i, j)
        return res

    def sampleFilter(self, locate, size):
        x, y = locate
        pixels = np.array([[self.getItem(i, j)
                            for j in range(y - size, y + size + 1)]
                           for i in range(x - size, x + size + 1)])
        # any idea to simplify it?
        return pixels.flatten()

    def sampleArray(self, locate, size):
        x, y = locate
        return np.array([[self.getItem(i, j)
                          for j in range(y - size, y + size + 1)]
                         for i in range(x - size, x + size + 1)])

    def sampleClockwise(self, locate):
        x, y = locate

        return [self._getItem(x - 1, y, 'const', 0),
                self._getItem(x - 1, y + 1, 'const', 0),
                self._getItem(x, y + 1, 'const', 0),
                self._getItem(x + 1, y + 1, 'const', 0),
                self._getItem(x + 1, y, 'const', 0),
                self._getItem(x + 1, y - 1, 'const', 0),
                self._getItem(x, y - 1, 'const', 0),
                self._getItem(x - 1, y - 1, 'const', 0)]

    def isSimplePoint(self, locate):
        x, y = locate
        if self.getItem(x, y) == 0:  # Should be a foreground pixel.
            return False

        sample = self.sampleClockwise(locate)
        l = len(sample)
        edges = 0
        vertices = 0

        evenIndex = [i * 2 for i in range(l // 2)]
        for e in evenIndex:
            k = 0
            signal = 0
            if sample[e] > 0:
                k += 1
            if sample[e + 1] > 0:
                k += 1
            if sample[(e + 2) % l] > 0:
                signal = 1  # the point e+2 shouldn't be count in.
                k += 1

            if k == 0 or k == 1:
                pass
            elif k == 2:
                edges += 1
            elif k == 3:
                edges += 2
            vertices += k - signal

        return vertices - edges == 1

    def isEndPoint(self, locate):  # for now, only accept the sample with 8 pixels.
        x, y = locate
        if self.getItem(x, y) == 0:  # Should be a foreground pixel.
            return False

        sample = self.sampleClockwise(locate)

        s = 0
        for a in sample:
            if a > 0:
                s += 1

        if s == 0 or s == 1:
            return True

        elif s == 2:
            for i in range(len(sample)):
                if sample[i] > 0:
                    if i == 0:
                        if sample[1] > 0 or sample[len(sample) - 1] > 0:
                            return True
                    elif i == len(sample) - 1:
                        if sample[0] > 0 or sample[len(sample) - 2] > 0:
                            return True
                    else:
                        if sample[i - 1] > 0 or sample[i + 1] > 0:
                            return True
                    return False
        return False

    def allEndPoints(self):
        for i in range(self.height):
            for j in range(self.width):
                if self.isSimplePoint((i, j)) and not self.isEndPoint((i, j)):
                    return False
        return True

    def getDistanceMap(self):
        # ====================calculate outside====================
        outMap = calcDistanceMap(self)
        # ====================calculate inside====================
        flippedImage = self.package(self.applyPrivateFunction(lambda x, y: flip(self.array[x][y])))
        inMap = calcDistanceMap(flippedImage) - 1
        # print(inMap, outMap, "DEBUG")
        return outMap - inMap

    def getGradientMap(self, dMap):
        if self.gradientMap is not None:
            return self.gradientMap

        res = np.empty((self.height, self.width), dtype=object)
        # sobel operator
        sobelVector1 = np.array([1, 2, 1])
        sobelVector2 = np.array([1, 0, -1])
        for i in range(self.height):
            for j in range(self.width):
                sample = dMap.sampleArray((i, j), 1)
                gX = np.dot(sobelVector1.T, np.dot(sobelVector2, sample))
                gY = np.dot(sobelVector2.T, np.dot(sobelVector1, sample))
                res[i][j] = (gX, gY)  # is it necessary to normalize the gradient?
        self.gradientMap = res
        return self.gradientMap

    def flux(self, x, y):
        f = 0
        for a, b in [(x - 1, y - 1), (x - 1, y), (x - 1, y + 1),
                     (x, y - 1), (x, y + 1),
                     (x + 1, y - 1), (x + 1, y), (x + 1, y + 1), ]:
            a, b = getLocation((a, b), self.array.shape, 'mirror')
            f += np.dot(self.gradientMap[a][b], np.array([a - x, b - y]))
            # print(f)

        return f / 8

    def getAverageFlux(self):
        self.getGradientMap(self.package(self.getDistanceMap()))
        res = self.applyPrivateFunction(self.flux)
        return res

    def package(self, array):
        return Image(array, self.border)

    def __sub__(self, other):
        res = np.zeros((self.height, self.width), dtype=int)
        for i in range(self.height):
            for j in range(self.width):
                res[i][j] = self.getItem(i, j) - other.getItem(i, j)
        return Image(res)

    def __add__(self, other):
        res = np.zeros((self.height, self.width), dtype=int)
        for i in range(self.height):
            for j in range(self.width):
                res[i][j] = self.getItem(i, j) + other.getItem(i, j)
        return Image(res)

    def __str__(self):
        res = ""
        res += "\n".join([" ".join([str(pixel) for pixel in e]) for e in self.array])
        return res

    def show(self):
        plt.imshow(self.array)
        plt.show()


if __name__ == '__main__':
    # img = Image(np.array([[0, 1, 2], [3, 4, 5]]), 'mirror')
    # print('Image with mirror edges')
    # print('================================================================')
    # print(img)
    # print('================================================================')
    # print('expected %d, got %d' % (3, img.width))
    # print('expected %d, got %d' % (2, img.height))
    # print('expected %d, got %d' % (4, img.getItem(1, 1)))  # normal [1, 1]
    # print('expected %d, got %d' % (4, img.getItem(-1, 1)))  # [-1, 1] -> [1, 1]
    # print('expected %d, got %d' % (4, img.getItem(3, 1)))  # [3, 1] -> [1, 1]
    # print('expected %d, got %d' % (3, img.getItem(1, 4)))  # [1, 4] -> [1, 0]
    # print('expected %d, got %d' % (3, img.getItem(1, -4)))  # [1, -4] -> [1, 0]
    # print('================================================================')
    # print(img.apply(lambda i, j, g: g.getItem(i, j) + 1))
    # print('================================================================')
    # img = Image(np.array([[0, 1, 2, 3], [4, 5, 6, 7], [8, 9, 10, 11], [12, 13, 14, 15]]), 'mirror')
    # print(img)
    # print(img.sampleFilter((0, 0), 1))
    # print('================================================================')
    # img = Image(np.array([[0, 1, 0, 0], [1, 1, 1, 1], [0, 1, 0, 1], [0, 0, 0, 1]]), 'mirror')
    # print(img)
    # print('================================================================')
    # print('expected True, got ' + str(img.isSimplePoint((0, 1))))  # normal
    # print('expected True, got ' + str(img.isSimplePoint((2, 1))))  # normal
    # print('expected False, got ' + str(img.isSimplePoint((2, 2))))  # not foreground
    # print('expected False, got ' + str(img.isSimplePoint((1, 2))))  # disconnected
    # print('expected False, got ' + str(img.isSimplePoint((1, 1))))  # create hole
    #
    # print('expected True, got ' + str(img.isEndPoint((3, 3))))  # normal
    # print('expected False, got ' + str(img.isEndPoint((2, 3))))  # v > 2
    # print('expected False, got ' + str(img.isEndPoint((1, 3))))  # not 4-adj
    # print(img.sampler)
    # print(img.getSample(), img.getSample(), img.getSample())
    # print(img.sampleClockwise((1, 1)))
    # print(img.sampleClockwise((3, 3)))
    img = Image(np.array([[0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                          [0, 1, 1, 1, 1, 1, 1, 1, 1, 0],
                          [0, 1, 1, 1, 1, 1, 1, 1, 1, 0],
                          [0, 1, 1, 1, 1, 1, 1, 1, 1, 0],
                          [0, 1, 1, 1, 1, 1, 1, 1, 1, 0],
                          [0, 1, 1, 1, 1, 1, 1, 1, 1, 0],
                          [0, 1, 1, 1, 1, 1, 1, 1, 1, 0],
                          [0, 1, 1, 1, 1, 1, 1, 1, 1, 0],
                          [0, 1, 1, 1, 1, 1, 1, 1, 1, 0],
                          [0, 1, 1, 1, 1, 1, 1, 1, 1, 0],
                          [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                          ]), 'zeroes')
    print(img)
    print("===================")
    print(Image(img.getDistanceMap()))
    # print("===================")
    # print(img.getGradientMap(img.package(img.getDistanceMap())))
    # print('expected 0, got ' + str(img.flux(2, 3)))
    # print("=====================================================")
    # img = Image(np.random.randint(2, size=(6, 6)))
    # print(img)
    # img.getDistanceMap()
    # img.getGradientMap()
    # Image(img.applyPrivateFunction(img.flux)).show()
    # img.show()
