#! /usr/bin/env python3
# -*- coding:utf-8 -*-

# Author by Alex
from utils.showAnswer import show_static as scr_s, show_dynamic as scr_d


class ImageSet(object):
    def __init__(self):
        self.images = []
        self.titles = []
        self.sf = None

    def addElement(self, image, title):
        self.images.append(image)
        self.titles.append(title)

    def setFunction(self, function):
        self.sf = function

    def show(self, title='BinaryImageProjects', typ='static', step=0.1):
        if typ == 'static':
            scr_s(self.images, self.titles, title)
        elif typ == 'dynamic':
            scr_d(self.sf, title, step)
