#! /usr/bin/env python3
# -*- coding:utf-8 -*-
import Structurings
from Image import Image
from Structurings import StructuringSquareElement as SE


# Author by Alex

def erosion(img: Image, size=1):
    se = SE(size, min)
    return img.applyPublicFunction(lambda i, j, g: se.calc((i, j), g))


def dilation(img: Image, size=1):
    se = SE(size, max)
    return img.applyPublicFunction(lambda i, j, g: se.calc((i, j), g))


def closing(img: Image, size=1):
    return erosion(dilation(img, size), size)


def opening(img: Image, size=1):
    return dilation(erosion(img, size), size)


def whiteTopHat(img: Image, size=1):
    return img - opening(img, size)


def blackTopHat(img: Image, size=1):
    return closing(img, size) - img


def hjSkeleton(img: Image, alpha=-0.5, gamma=10):
    fluxMap = img.getAverageFlux()
    return img.applyPublicFunction(lambda i, j, g: gamma if fluxMap[i][j] < alpha else 0)


if __name__ == "__main__":
    from utils.readBPG import read_file
    from ImageSet import ImageSet

    iSet = ImageSet()

    cimg = read_file('../output/test.bpg')
    iSet.addElement(cimg, 'origin')
    iSet.addElement(cimg.package(cimg.getAverageFlux()), 'AverageFluxImage')
    iSet.addElement(cimg - hjSkeleton(cimg, -4.2, 3), 'Hamilton-Jacobi')
    # iSet.add_element(blackTopHat(cimg, 10), 'bth')
    # iSet.add_element(closing(cimg, 10), 'closing')
    # iSet.add_element(opening(cimg, 10), 'opening')

    iSet.show()
