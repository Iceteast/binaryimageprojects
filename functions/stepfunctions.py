import abc


class StepFunction(metaclass=abc.ABCMeta):

    @abc.abstractmethod
    def __init__(self, image):
        self.image = image

    @abc.abstractmethod
    def step(self):
        ...


class SillySkeleton(StepFunction):

    def __init__(self, image):
        self.image = image

    def step(self):
        if self.image.allEndPoints():
            return False

        x, y = self.image.getSample()
        if x is None:
            return False

        while not self.image.isSimplePoint((x, y)) or self.image.isEndPoint((x, y)):
            x, y = self.image.getSample()
            if x is None:
                return False

        self.image.setItem(x, y, 0)
        return True


class FluxOrderedThinning(StepFunction):
    def __init__(self, image, flux):
        self.image = image
        self.flux = flux
        self.fluxMap = image.getAverageFlux()

    def step(self):
        pass


if __name__ == '__main__':
    from Image import Image
    import numpy as np

    img = Image(np.array([[1, 1, 1, 1, 1, 1],
                          [1, 1, 1, 1, 1, 1],
                          [1, 1, 1, 1, 1, 1],
                          [1, 1, 1, 1, 1, 1],
                          [1, 1, 1, 1, 1, 1]]))
    s = SillySkeleton(img)
    print(s.image)
    while s.step():
        print("===========")
        print(s.image)
