#! /usr/bin/env python3
# -*- coding:utf-8 -*-
import time

# Author by Alex
from utils.readBPG import read_file
from cfg import *
from ImageSet import ImageSet
# from functions import basics
from functions.stepfunctions import SillySkeleton
iSet = ImageSet()

origin = read_file(FILE_PATH)
iSet.addElement(origin, 'origin')
iSet.addElement(origin.package(origin.getAverageFlux()), 'flux')
# iSet.addElement(basics.closing(origin, 3), 'closing')
# iSet.addElement(basics.blackTopHat(origin, 3), 'bth')
iSet.show()

# sf = SillySkeleton(origin)
# iSet.setFunction(sf)
# iSet.show("get Skeleton", "dynamic", 0.001)
