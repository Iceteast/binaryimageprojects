# Binary Image Projects

## About
This is a set of several programs, which are introduced in the Mathematical Morphological Image Analysis (MMIA) lecture.

## BPG Format
It's a pure text image format with strictly spaces.

- First line must be 'BPG', just like PGM format.
- Second line is the width and the height of the image.
- Then is the gray values of image, split with space and line break.

## Packages
- numpy(of course)
- skimage(I use the `io` function to read a jpeg file)
- matplotlib(for "temporary" output. yes, there is no viewer for my poor image format.)

## Run
each file has a simple runnable example or tests.
Or you can use `convertBPG.py` to convert a JPG file to get a BPG file.
Then using it in `main.py`.