#! /usr/bin/env python3
# -*- coding:utf-8 -*-

# Author by Alex

# This file is only for testing new ideas. Please just ignore it.

import numpy as np

au = np.array([[0, 0, 0], [1, 1, 1], [1, 1, 1]])
ad = np.array([[1, 1, 1], [1, 1, 1], [0, 0, 0]])
al = np.array([[0, 1, 1], [0, 1, 1], [0, 1, 1]])
ar = np.array([[1, 1, 0], [1, 1, 0], [1, 1, 0]])
ul = np.array([[0, 0, 0], [0, 1, 1], [0, 1, 1]])
ur = np.array([[0, 0, 0], [1, 1, 0], [1, 1, 0]])
dl = np.array([[0, 1, 1], [0, 1, 1], [0, 0, 0]])
dr = np.array([[1, 1, 0], [1, 1, 0], [0, 0, 0]])
li = [au, ad, al, ar, ul, ur, dl, dr]
m1 = np.array([1, 2, 1])
m2 = np.array([1, 0, -1])

for i in li:
    x = np.dot(m1.T, np.dot(m2, i))
    y = np.dot(m2.T, np.dot(m1, i))
    print("%s : (%d, %d)" % (i, x, y))


