#! /usr/bin/env python3
# -*- coding:utf-8 -*-

from Image import Image
from typing import Callable


# Author by Alex

class StructuringFunction:

    def calc(self, locate, img):
        ...


class StructuringSquareElement(StructuringFunction):

    def __init__(self, size, func: Callable[[list[int]], int]):
        self.size = size
        self.func = func

    def calc(self, locate, img: Image) -> int:
        return self.func(img.sampleFilter(locate, self.size))


if __name__ == '__main__':
    import numpy as np
    se = StructuringSquareElement(1, min)
    image = Image(np.array([[1, 1, 1], [1, 0, 1], [1, 1, 1]]), 'mirror')
    print("expected %d, got %s" % (0, se.calc((2, 1), image)))
