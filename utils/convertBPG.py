#! /usr/bin/env python3
# -*- coding:utf-8 -*-

# Author by Alex
from skimage import io


def normalize(x: float) -> int:
    if x < 0.5:
        return 0
    else:
        return 1


def write_bpg(image, filename='out.bpg'):
    h = len(image)
    w = len(image[0])
    print("[LOG]%d - %d" % (w, h))
    ans = "\n".join([" ".join([str(normalize(pixel)) for pixel in e]) for e in image])
    with open(filename, 'w') as f:
        f.write("BPG\n")
        f.write("%d %d\n" % (w, h))
        f.write(ans)
    print("[LOG] %s is created!" % filename)


def convert_file(filename, output='../output/test.bpg'):
    jpg = io.imread(filename, as_gray=True)
    write_bpg(jpg, output)
    return jpg


if __name__ == '__main__':
    import matplotlib.pyplot as plt
    img = convert_file('../resources/witch.pgm')
    plt.imshow(img)
    plt.show()
