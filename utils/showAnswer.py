#! /usr/bin/env python3
# -*- coding:utf-8 -*-


# Author by Alex
import matplotlib.pyplot as plt
from math import sqrt, ceil
import numpy as np
import time
from Image import Image


def show_static(images, titles, title):
    if len(images) != len(titles):
        raise ValueError("Unsuitable number of images and titles")
    size = len(images)

    fig = plt.figure(figsize=(15, 15))
    fig.subplots_adjust(wspace=0.1)
    for i in range(size):
        if size > 4:
            w = ceil(sqrt(size))
            fig.add_subplot(w, w, i + 1).imshow(images[i].array)
        else:
            fig.add_subplot(1, size, i + 1).imshow(images[i].array)

        plt.axis("off")
        plt.title(titles[i])

    plt.suptitle(title)
    plt.show()


def show_dynamic(sf, title, step=0.1):
    imdata = sf.image.array
    plt.ion()
    figure, ax = plt.subplots()
    figure.suptitle(title)
    im = ax.imshow(imdata, vmin=0, vmax=1)
    while sf.step():
        im.set_data(sf.image.array)
        figure.canvas.draw()
        figure.canvas.flush_events()
        time.sleep(step)

    plt.ioff()
    plt.show()


if __name__ == "__main__":

    from random import randint
    from functions.stepfunctions import SillySkeleton

    # img = [Image(np.array([[randint(0, 255) for _ in range(5)] for _ in range(5)])) for _ in range(3)]
    # show_static(img, ['a', 'b', 'c'], 'test')

    img = Image(np.array([[randint(1, 1) for _ in range(10)] for _ in range(10)]))
    ssf = SillySkeleton(img)
    show_dynamic(ssf, "test")
