#! /usr/bin/env python3
# -*- coding:utf-8 -*-

# Author by Alex

MAX_INT = 2147483647
import numpy as np


def flip(x):
    return x ^ 1


def calcDistanceMap(img):
    if img.distanceMap is not None:
        return img.distanceMap

    # initialize the distance map
    res = np.full((img.height, img.width), MAX_INT, dtype=int)
    for i in range(img.height):
        for j in range(img.width):
            if img.array[i][j] > 0:
                res[i][j] = 0

    # forward update
    for i in range(img.height):
        for j in range(img.width):
            if res[i][j] == MAX_INT:  # unassigned pixels don't need to be considered.
                continue
            if j + 1 < img.width:
                res[i][j + 1] = min(res[i][j + 1], res[i][j] + 1)
            if j + 1 < img.width and i + 1 < img.height:
                res[i + 1][j + 1] = min(res[i + 1][j + 1], res[i][j] + 2)
            if i + 1 < img.height:
                res[i + 1][j] = min(res[i + 1][j], res[i][j] + 1)
            if i + 1 < img.height and j - 1 >= 0:
                res[i + 1][j - 1] = min(res[i + 1][j - 1], res[i][j] + 2)

    # backward update
    for i in range(img.height - 1, -1, -1):
        for j in range(img.width - 1, -1, -1):
            if res[i][j] == MAX_INT:  # unassigned pixels don't need to be considered.
                continue
            if j - 1 >= 0:
                res[i][j - 1] = min(res[i][j - 1], res[i][j] + 1)
            if j + 1 < img.width and i - 1 >= 0:
                res[i - 1][j + 1] = min(res[i - 1][j + 1], res[i][j] + 2)
            if i - 1 >= 0:
                res[i - 1][j] = min(res[i - 1][j], res[i][j] + 1)
            if i - 1 >= 0 and j - 1 >= 0:
                res[i - 1][j - 1] = min(res[i - 1][j - 1], res[i][j] + 2)

    img.distanceMap = res
    return img.distanceMap


def decoordinate(w: int, i: int) -> (int, int):
    return i // w, i % w


def getLocation(locate, shape, typ="mirror"):
    h, w = shape
    x, y = locate

    if typ == 'mirror':
        if x < 0:
            return getLocation((-x, y), shape, typ)
        elif x >= h:
            return getLocation(((h - 1) * 2 - x, y), shape, typ)

        if y < 0:
            return getLocation((x, -y), shape, typ)
        elif y >= w:
            return getLocation((x, (w - 1) * 2 - y), shape, typ)

    elif typ == 'extend':
        if x < 0:
            return getLocation((0, y), shape, typ)
        elif x >= h:
            return getLocation((h - 1, y), shape, typ)

        if y < 0:
            return getLocation((x, 0), shape, typ)
        elif y >= w:
            return getLocation((x, w - 1), shape, typ)

    elif typ == 'repeat':
        return x % h, y % w
    elif typ == 'const':
        if x < 0 or x >= h or y < 0 or y >= w:
            return None, None
    else:
        raise ValueError('Invalid border type: %s' % typ)

    return x, y


if __name__ == "__main__":
    import numpy as np

    s = np.array([[3, 5, 2], [1, 2, 3], [4, 4, 4], [4, 4, 4]])
    print(s.shape)
    print(getLocation((7, 2), s.shape))
