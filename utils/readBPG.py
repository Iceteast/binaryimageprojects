#! /usr/bin/env python3
# -*- coding:utf-8 -*-

# Author by Alex
import numpy as np
import matplotlib.pyplot as plt

from Image import Image


def read_file(filename):
    with open(filename) as f:
        c = next(f).rstrip()
        # validation
        if c != 'BPG':
            raise ValueError('Invalid file header in %r with : %s' % (filename, c))
        w, h = [int(x) for x in next(f).split()]  # read first line
        array = []
        for line in f:  # read rest of lines
            array.append([int(x) for x in line.split()])
        if w != len(array[0]) and h != len(array):
            raise ValueError('Invalid image size! expected [%d, %d], got [%d, %d]' % (w, h, len(array[0]), len(array)))
        return Image(np.array(array))


if __name__ == '__main__':
    img = read_file('../output/test.bpg')
    img.show()
